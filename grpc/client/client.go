package grpc_client

import (
	"branch/config"
	"branch/genproto/catalog_service"
	"branch/genproto/sale_service"
	"branch/genproto/staff_service"
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// GrpcClientI ...
type GrpcClientI interface {
	TarifService() staff_service.TarifServerClient
	StaffService() staff_service.StaffServerClient
	SaleService() sale_service.SaleServerClient
	SaleProductService() sale_service.SaleProductServerClient
	BranchTransactionService() sale_service.BranchTransactionServerClient
	TransactionService() sale_service.TransactionServerClient
	// CatagoryService() catalog_service.CategoryServiceClient
	// ProductService() catalog_service.ProductServiceClient
}

// GrpcClient ...
type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

// New ...
func New(cfg config.Config) (*GrpcClient, error) {

	connSale, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.SaleServiceHost, cfg.SaleServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("sale service dial host: %s port:%d err: %s",
			cfg.SaleServiceHost, cfg.SaleServisePort, err)
	}

	connStaff, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.StaffServiceHost, cfg.StaffServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("staff service dial host: %s port:%d err: %s",
			cfg.StaffServiceHost, cfg.StaffServisePort, err)
	}

	connCatalog, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.CatalogServiceHost, cfg.CatalogServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("courier service dial host: %s port:%d err: %s",
			cfg.CatalogServiceHost, cfg.CatalogServisePort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"staff_service":              staff_service.NewStaffServerClient(connStaff),
			"tarif_service":              staff_service.NewTarifServerClient(connStaff),
			"sale_service":               sale_service.NewSaleServerClient(connSale),
			"sale_product_service":       sale_service.NewSaleProductServerClient(connSale),
			"branch_transaction_service": sale_service.NewBranchTransactionServerClient(connSale),
			"transaction_service":        sale_service.NewTransactionServerClient(connSale),
			"category_service":           catalog_service.NewCategoryServiceClient(connCatalog),
			"product_service":            catalog_service.NewProductServiceClient(connCatalog),
		},
	}, nil
}

func (g *GrpcClient) TarifService() staff_service.TarifServerClient {
	return g.connections["tarif_service"].(staff_service.TarifServerClient)
}

func (g *GrpcClient) StaffService() staff_service.StaffServerClient {
	return g.connections["staff_service"].(staff_service.StaffServerClient)
}

func (g *GrpcClient) SaleService() sale_service.SaleServerClient {
	return g.connections["sale_service"].(sale_service.SaleServerClient)
}

func (g *GrpcClient) SaleProductService() sale_service.SaleProductServerClient {
	return g.connections["sale_product_service"].(sale_service.SaleProductServerClient)
}

func (g *GrpcClient) BranchTransactionService() sale_service.BranchTransactionServerClient {
	return g.connections["branch_transaction_service"].(sale_service.BranchTransactionServerClient)
}

func (g *GrpcClient) TransactionService() sale_service.TransactionServerClient {
	return g.connections["transaction_service"].(sale_service.TransactionServerClient)
}

// func (g *GrpcClient) CategoryService() catalog_service.CategoryServiceClient {
// 	return g.connections["category_service"].(catalog_service.CategoryServiceClient)
// }

// func (g *GrpcClient) ProductService() catalog_service.ProductServiceClient {
// 	return g.connections["product_service"].(catalog_service.ProductServiceClient)
// }
