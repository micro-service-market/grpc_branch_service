package service

import (
	"branch/genproto/branch_service"
	grpc_client "branch/grpc/client"
	"branch/packages/logger"
	"branch/storage"
	"context"
)

type BranchProductService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	branch_service.UnimplementedBranchProductServiceServer
}

func NewBranchProductService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *BranchProductService {
	return &BranchProductService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (b *BranchProductService) Create(ctx context.Context, req *branch_service.CreateBranchProduct) (*branch_service.IdReqRes, error) {
	id, err := b.storage.BranchProduct().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &branch_service.IdReqRes{Id: id}, nil
}

func (b *BranchProductService) Update(ctx context.Context, req *branch_service.BranchProduct) (*branch_service.ResponseString, error) {
	str, err := b.storage.BranchProduct().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &branch_service.ResponseString{Text: str}, nil
}

func (b *BranchProductService) Get(ctx context.Context, req *branch_service.IdReqRes) (*branch_service.BranchProduct, error) {
	branchProduct, err := b.storage.BranchProduct().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return branchProduct, nil
}

func (b *BranchProductService) GetAll(ctx context.Context, req *branch_service.GetAllBranchProductRequest) (*branch_service.GetAllBranchProductResponse, error) {
	branchProducts, err := b.storage.BranchProduct().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return branchProducts, nil
}

func (b *BranchProductService) Delete(ctx context.Context, req *branch_service.IdReqRes) (*branch_service.ResponseString, error) {
	text, err := b.storage.BranchProduct().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &branch_service.ResponseString{Text: text}, nil
}

func (b *BranchProductService) GetProductByProductId(ctx context.Context, req *branch_service.GetProductRequest) (*branch_service.BranchProduct, error) {
	branchProduct, err := b.storage.BranchProduct().GetProduct(ctx, req)
	if err != nil {
		return nil, err
	}

	return branchProduct, nil
}
