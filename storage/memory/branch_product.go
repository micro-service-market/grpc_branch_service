package memory

import (
	"branch/genproto/branch_service"
	"branch/packages/helper"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type branchProductRepo struct {
	db *pgxpool.Pool
}

func NewBranchProductRepo(db *pgxpool.Pool) *branchProductRepo {

	return &branchProductRepo{
		db: db,
	}

}

func (b *branchProductRepo) Create(ctx context.Context, req *branch_service.CreateBranchProduct) (string, error) {

	id := uuid.NewString()

	query := `
	INSERT INTO 
		branch_products(id,product_id,branch_id,count) 
	VALUES($1,$2,$3,$4)`

	_, err := b.db.Exec(ctx, query,
		id,
		req.ProductId,
		req.BranchId,
		req.Count,
	)
	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil

}

func (b *branchProductRepo) Update(ctx context.Context, req *branch_service.BranchProduct) (string, error) {

	query := `
	UPDATE branch_products
	SET product_id=$2,branch_id=$3,count=$4,updated_at=NOW()
	WHERE id=$1`

	resp, err := b.db.Exec(ctx, query,
		req.Id,
		req.ProductId,
		req.BranchId,
		req.Count,
	)
	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}
	return "OK", nil
}

func (b *branchProductRepo) Get(ctx context.Context, req *branch_service.IdReqRes) (*branch_service.BranchProduct, error) {

	query := `
	SELECT
	id,
	product_id,
	branch_id,
	count,
	created_at::text,
	updated_at::text
	FROM branch_products WHERE id = $1`

	resp := b.db.QueryRow(ctx, query, req.Id)

	var branch branch_service.BranchProduct

	err := resp.Scan(
		&branch.Id,
		&branch.ProductId,
		&branch.BranchId,
		&branch.Count,
		&branch.CreatedAt,
		&branch.UpdatedAt,
	)

	if err != nil {
		return &branch_service.BranchProduct{}, err
	}

	return &branch, nil
}

func (b *branchProductRepo) GetAll(ctx context.Context, req *branch_service.GetAllBranchProductRequest) (*branch_service.GetAllBranchProductResponse, error) {

	var (
		params  = make(map[string]interface{})
		filter  = " WHERE true"
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)
	s := `
	SELECT
	id,
	product_id,
	branch_id,
	count,
	created_at::text,
	updated_at::text
	FROM branch_products WHERE id = $1`
	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM
		branch_products
	`

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.GetLimit())
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ
	countQuery := cQ + filter

	q, pArr := helper.ReplaceQueryParams(query, params)
	rows, err := b.db.Query(ctx, q, pArr...)
	if err != nil {
		return &branch_service.GetAllBranchProductResponse{}, err
	}

	err = b.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &branch_service.GetAllBranchProductResponse{}, err
	}

	defer rows.Close()

	result := []*branch_service.BranchProduct{}

	for rows.Next() {

		var branch branch_service.BranchProduct

		err := rows.Scan(
			&branch.Id,
			&branch.ProductId,
			&branch.BranchId,
			&branch.Count,
			&branch.CreatedAt,
			&branch.UpdatedAt,
		)
		if err != nil {
			return &branch_service.GetAllBranchProductResponse{}, err
		}

		result = append(result, &branch)

	}

	return &branch_service.GetAllBranchProductResponse{BranchProducts: result, Count: int64(count)}, nil

}

func (b *branchProductRepo) Delete(ctx context.Context, req *branch_service.IdReqRes) (string, error) {

	query := `DELETE FROM branch_products WHERE id = $1`

	resp, err := b.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (b *branchProductRepo) GetProduct(ctx context.Context, req *branch_service.GetProductRequest) (*branch_service.BranchProduct, error) {

	query := `
	SELECT
	id,
	product_id,
	branch_id,
	count,
	created_at::text,
	updated_at::text
	FROM branch_products WHERE branch_id = $1 AND product_id=$2`

	resp := b.db.QueryRow(ctx, query, req.BranchId, req.ProductId)

	var branch branch_service.BranchProduct

	err := resp.Scan(
		&branch.Id,
		&branch.ProductId,
		&branch.BranchId,
		&branch.Count,
		&branch.CreatedAt,
		&branch.UpdatedAt,
	)

	if err != nil {
		return &branch_service.BranchProduct{}, err
	}

	return &branch, nil
}
