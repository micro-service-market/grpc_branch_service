package storage

import (
	"branch/genproto/branch_service"
	"context"
)

type StoregeI interface {
	Branch() BranchesI
	BranchProduct() BranchProductI
}

type BranchesI interface {
	Create(context.Context, *branch_service.CreateBranch) (string, error)
	Update(context.Context, *branch_service.Branch) (string, error)
	GetBranch(context.Context, *branch_service.IdReqRes) (*branch_service.Branch, error)
	GetAllBranch(context.Context, *branch_service.GetAllBranchRequest) (*branch_service.GetAllBranchResponse, error)
	DeleteBranch(context.Context, *branch_service.IdReqRes) (string, error)
}

type BranchProductI interface {
	Create(context.Context, *branch_service.CreateBranchProduct) (string, error)
	Update(context.Context, *branch_service.BranchProduct) (string, error)
	Get(context.Context, *branch_service.IdReqRes) (*branch_service.BranchProduct, error)
	GetAll(context.Context, *branch_service.GetAllBranchProductRequest) (*branch_service.GetAllBranchProductResponse, error)
	Delete(context.Context, *branch_service.IdReqRes) (string, error)
	GetProduct(context.Context, *branch_service.GetProductRequest) (*branch_service.BranchProduct, error)
}
